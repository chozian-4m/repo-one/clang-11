# Clang/LLVM

## Clang compiler

The Clang project provides a language front-end and tooling infrastructure for languages in the C language family (C, C++, Objective C/C++, OpenCL, CUDA, and RenderScript) for the LLVM project. Both a GCC-compatible compiler driver (clang) and an MSVC-compatible compiler driver (clang-cl.exe) are provided. You can get and build the source today.

[LLVM project](https://llvm.org/)

[Clang project](https://clang.llvm.org/)

## Clang-tools

- clang-analyzer
- clang-tidy
- clang-format
- clang-apply-replacements
- clang-change-namespace
- clang-check
- clang-doc
- clang-extdef-mapping
- clang-include-fixer
- clang-move
- clang-offload-bundler
- clang-offload-wrapper
- clang-query
- clang-refactor
- clang-rename
- clang-reorder-fields
- clang-scan-deps

Code in the LLVM project is licensed under the "Apache 2.0 License with LLVM exceptions"
