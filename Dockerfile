ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

COPY gpg/centos8-gpg.asc .
COPY llvm-libs-11.0.0-2.module_el8.4.0+587+5187cac0.x86_64.rpm /llvm-rpm/llvm-libs-11.0.0-2.module_el8.4.0+587+5187cac0.x86_64.rpm
COPY clang-libs-11.0.0-1.module_el8.4.0+587+5187cac0.x86_64.rpm /llvm-rpm/clang-libs-11.0.0-1.module_el8.4.0+587+5187cac0.x86_64.rpm
COPY clang-11.0.0-1.module_el8.4.0+587+5187cac0.x86_64.rpm /llvm-rpm/clang-11.0.0-1.module_el8.4.0+587+5187cac0.x86_64.rpm
COPY clang-analyzer-11.0.0-1.module_el8.4.0+587+5187cac0.noarch.rpm /llvm-rpm/clang-analyzer-11.0.0-1.module_el8.4.0+587+5187cac0.noarch.rpm
COPY clang-tools-extra-11.0.0-1.module_el8.4.0+587+5187cac0.x86_64.rpm /llvm-rpm/clang-tools-extra-11.0.0-1.module_el8.4.0+587+5187cac0.x86_64.rpm

RUN dnf clean packages && \
    dnf update --setopt=tsflags=nodocs -y && \
	dnf install -y cmake make && \
	dnf clean all

RUN rpm --import centos8-gpg.asc && \
    dnf install -y \
        /llvm-rpm/llvm-libs-11.0.0-2.module_el8.4.0+587+5187cac0.x86_64.rpm \
        /llvm-rpm/clang-libs-11.0.0-1.module_el8.4.0+587+5187cac0.x86_64.rpm \
        /llvm-rpm/clang-11.0.0-1.module_el8.4.0+587+5187cac0.x86_64.rpm \
        /llvm-rpm/clang-analyzer-11.0.0-1.module_el8.4.0+587+5187cac0.noarch.rpm \
        /llvm-rpm/clang-tools-extra-11.0.0-1.module_el8.4.0+587+5187cac0.x86_64.rpm && \
    rm -rf /llvm-rpm

WORKDIR /clang    
RUN useradd -u 1001 -g 0 -M -d /clang default
ENV USER=default
USER 1001

ENTRYPOINT ["/bin/bash"]
